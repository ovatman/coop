/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coop.DWorkDistribution;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author ovatman
 */
public class DistributionTester {

    private final ExecutorService exec = Executors.newCachedThreadPool();
    //private final ExecutorService exec = Executors.newFixedThreadPool(4);
    
    
    public void testDistribution() throws InterruptedException, ExecutionException {
        Future<Integer> result = exec.submit(new FiboWorker(17));

        long start=System.currentTimeMillis();
        int res=result.get();
        
        
        System.out.println(res);
        System.out.println("It took: "+(System.currentTimeMillis()-start));
        exec.shutdown();
    }

    private class FiboWorker implements Callable<Integer> {

        int arg;

        public FiboWorker(int n) {
            arg = n;
        }

        @Override
        public Integer call() throws Exception {
            Thread.sleep(100);
            if (arg > 2) {
                Future<Integer> left = exec.submit(new FiboWorker(arg - 1));
                Future<Integer> right = exec.submit(new FiboWorker(arg - 2));
                return left.get() + right.get();
            } else {
                return 1;
            }
        }

    }

}
