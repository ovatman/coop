/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coop;

import coop.AThreadBasics.ThreadTester;
import coop.BJavaConcurrency.ConcurrencyTester;
import coop.CRegisters.RegisterTester;
import coop.DWorkDistribution.DistributionTester;
import coop.EExamQuestion.QuestionTester;
import coop.FStreams.StreamTester;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.ExecutionException;

/**
 *
 * @author ovatman
 */
public class Tester {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException, BrokenBarrierException, ExecutionException {
        //ThreadTester t=new ThreadTester();
        //t.testCreation();
        //t.testSharedVar();
        //t.testSharedArray();
        //t.testLockedArray();
        
        //ConcurrencyTester t = new ConcurrencyTester();
        //t.testCountDownLatch();
        //t.testCyclicBarrier();
        //t.testExecutor();
        //t.testInterruption();
        //t.testFutures();
        
        //RegisterTester t = new RegisterTester();
        //t.testRegister();
        
        //DistributionTester t = new DistributionTester();
        //t.testDistribution();
        
        
        //QuestionTester t = new QuestionTester();
        //t.testQuestion();
        
        StreamTester t = new StreamTester();
        t.testStreams();
    }
    
    
    
}
