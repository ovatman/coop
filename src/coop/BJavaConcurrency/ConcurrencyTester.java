/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coop.BJavaConcurrency;

import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Exchanger;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ovatman
 */
public class ConcurrencyTester {

    public void testCountDownLatch() throws InterruptedException {

        final SharedArray arr = new SharedArray(10);

        final CountDownLatch startSignal = new CountDownLatch(1);
        final CountDownLatch doneSignal = new CountDownLatch(10);

        for (int i = 0; i < 10; ++i) {
            new Thread() {
                public void run() {
                    try {
                        System.out.println("Thread " + Thread.currentThread().getName() + " waiting");
                        startSignal.await();
                        System.out.println("Thread " + Thread.currentThread().getName() + " started");

                        int[] arrCpy = arr.getRow(((int) Thread.currentThread().getId()) % 10);

                        for (int i = 0; i < arrCpy.length; i++) {
                            Thread.sleep((int) (Math.random() * 50));
                            arrCpy[i] %= 2;
                        }
                        System.out.println("Thread " + Thread.currentThread().getName() + " done");
                        doneSignal.countDown();
                    } catch (InterruptedException ie) {
                    }
                }
            }.start();
        }

        arr.fillRandom();
        startSignal.countDown();
        doneSignal.await();
        arr.printArray();
    }

    public void testCyclicBarrier() throws InterruptedException {

        final SharedArray arr = new SharedArray(10);
        final CyclicBarrier barrier = new CyclicBarrier(10);

        Runnable r = new Runnable() {

            @Override
            public void run() {
                try {
                    int[] arrCpy = arr.getRow(((int) Thread.currentThread().getId()) % 10);

                    for (int i = 0; i < arrCpy.length; i++) {
                        Thread.sleep((int) (Math.random() * 50));
                        arrCpy[i]++;
                    }

                    barrier.await();
                } catch (InterruptedException ex) {
                    Logger.getLogger(ConcurrencyTester.class.getName()).log(Level.SEVERE, null, ex);
                } catch (BrokenBarrierException ex) {
                    Logger.getLogger(ConcurrencyTester.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };

        Thread[] threads = new Thread[10];

        for (int j = 0; j < 10; j++) {
            for (int i = 0; i < 10; i++) {
                threads[i] = new Thread(r);
                threads[i].start();
            }

            for (int i = 0; i < 10; i++) {
                threads[i].join();
            }

            arr.printArray();
        }
    }

    public void testExecutor() throws InterruptedException, BrokenBarrierException {
        final int N = 10;

        final SharedArray arr = new SharedArray(N);

        final CyclicBarrier barrier = new CyclicBarrier(N + 1);

        Runnable r = new Runnable() {

            @Override
            public void run() {
                try {
                    int[] arrCpy = arr.getRow(((int) Thread.currentThread().getId()) % N);

                    for (int i = 0; i < arrCpy.length; i++) {
                        Thread.sleep((int) (Math.random() * 50));
                        arrCpy[i]++;
                    }

                    barrier.await();
                } catch (InterruptedException ex) {
                    Logger.getLogger(ConcurrencyTester.class.getName()).log(Level.SEVERE, null, ex);
                } catch (BrokenBarrierException ex) {
                    Logger.getLogger(ConcurrencyTester.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };

        //ExecutorService e = Executors.newFixedThreadPool(N);
        ExecutorService e = Executors.newCachedThreadPool();

        for (int j = 0; j < 30; j++) {
            for (int i = 0; i < N; i++) {
                e.execute(r);
            }

            barrier.await();
            arr.printArray();
        }

        
        e.shutdown();

    }

    public void testInterruption() throws InterruptedException {

        Exchanger<String> napkin = new Exchanger<String>();
        String flag = new String("flag");

        Thread[] threads = new Thread[10];

        for (int i = 0; i < 10; i++) {
            threads[i] = new Runner(napkin);
            threads[i].start();
        }

        napkin.exchange(flag);
        System.out.println("Start!!!");
        Thread.sleep(2000+(int) (Math.random() * 10000));
        
        System.out.println("Stop!!!");
        for (int i = 0; i < 10; i++) {
            threads[i].interrupt();
        }
        
        for (int i = 0; i < 10; i++) {
            threads[i].join();
        }

    }

    public void testFutures() throws InterruptedException, ExecutionException {
        SharedArray arr=new SharedArray(100000);
        arr.fillRandom();
        
        int chunks=1000;
        ArrayList<Future<Integer>> sums=new ArrayList<Future<Integer>>();
        
        //ExecutorService s = Executors.newCachedThreadPool();
        //ExecutorService s = Executors.newSingleThreadExecutor();
        ExecutorService s = Executors.newFixedThreadPool(chunks);
        
        Long start=System.currentTimeMillis();
        for(int i=1;i<=chunks;i++){
            sums.add(s.submit(new Accumulator(arr.getChunk(i+1, chunks))));
        }
        
        int grandSum=0;
        
        for(Future<Integer> f:sums)
            grandSum+=f.get();
        
        Long end=System.currentTimeMillis();
        
        s.shutdown();
        
        System.out.println("GrandSum:"+grandSum);
            
        System.out.println("Time:"+((double)(end-start)/1000));
        
        
    }
       
    private class Runner extends Thread {

        private Exchanger<String> napkin;

        public Runner(Exchanger napkin) {
            this.napkin = napkin;
        }

        public void run() {
            String flag="empty";
            try {
                while (true) {
                    //Thread.sleep((int) (Math.random() * 10));
                    
                    flag=napkin.exchange(flag);
                    //if(flag.equals("flag"))
                    //    System.out.println(Thread.currentThread().getId()+" got the flag");

                }
            } catch (InterruptedException ex) {
                    if(flag.equals("flag"))
                        System.out.println(Thread.currentThread().getId()+" got the flag");
            }
        }

    };

    private class Accumulator implements Callable{
        private int[] array;

        public Accumulator(int[] array) {
            this.array = array;
        }
        
        @Override
        public Integer call() throws Exception {
            int sum=0;
            for(int i:array)
                sum+=i;
            
            return sum;
        }
    
    
    }
}
