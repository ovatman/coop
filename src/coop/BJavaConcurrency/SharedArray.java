/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coop.BJavaConcurrency;

import java.util.Arrays;

/**
 *
 * @author ovatman
 */
public class SharedArray {
    private int[][] anArray;

    public SharedArray(int N) {
        anArray=new int[N][10];
    }
    
    public void fillRandom(){
    
        for(int i=0;i<anArray.length;i++)
            for(int j=0;j<anArray[i].length;j++)
                anArray[i][j]=(int)(Math.random()*10);
    }
    
    public void printArray(){
        for(int[] arr:anArray)
            System.out.println(Arrays.toString(arr));
    }
    
    public int[] getRow(int i){
        return anArray[i];
    }
    
    public int[] getChunk(int nom,int denom){
        
        int chunkSize=(anArray.length*10)/denom;
        
        int startRow=((nom-1)*chunkSize)/10;
        int startCol=((nom-1)*chunkSize)%10;
        
        if(nom == denom)
            chunkSize+=(anArray.length*10)%denom;
        
        
        int[] chunk = new int[chunkSize];
                        
        int count=0;
        
        for(int i=startRow;i<anArray.length;i++){
            for(int j=startCol;j<anArray[i].length;j++){
                if(count>=chunkSize)
                    break;
                chunk[count++]=anArray[i][j];
                startCol=0;
            }
            startRow=0;
        }
        
        return chunk;
    }
        
    
}
