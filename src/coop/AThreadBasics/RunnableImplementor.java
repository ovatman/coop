/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coop.AThreadBasics;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ovatman
 */
public class RunnableImplementor implements Runnable{

    private String name;
    
    @Override
    public void run() {
        try {
            
            
            
            Thread.sleep(20);
            System.out.println("I am:"+name);
            
            
            
            
        } catch (InterruptedException ex) {
            Logger.getLogger(RunnableImplementor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
}
