/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coop.AThreadBasics;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ovatman
 */
public class SharedArray{
    private int[] values;

    public SharedArray() {
        values=new int[10];
    }

    
    public int[] getValues() {
        return values;
    }

    public synchronized void increment() throws InterruptedException {
        for(int i=0;i<values.length;i++){
            values[i]++;
            Thread.sleep(10);
        }
    }
    
    
}
