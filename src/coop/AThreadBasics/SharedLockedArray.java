/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coop.AThreadBasics;

import java.util.Arrays;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ovatman
 */
public class SharedLockedArray implements Runnable{
    private int[] values;
    private boolean display;
    private boolean stop;
    private boolean dirty;
    private ReentrantLock aLock;
    private Condition readCond;
    private Condition writeCond;
    
    public SharedLockedArray() {
        values=new int[10];
    }

    public Lock getaLock() {
        return aLock;
    }

    public void setaLock(ReentrantLock aLock) {
        this.aLock = aLock;
    }

    
    
    public int[] getValues() {
        return values;
    }

    @Override
    public void run() {
        try {
            aLock.lock();
            writeCond.signal();
            aLock.unlock();
            
            while (!stop) {

                if (display) {
                    
                    aLock.lock();
                    while(!dirty)
                        readCond.await();
                    System.out.println("Shared:"+Arrays.toString(values));
                    dirty=false;
                    writeCond.signal();
                    aLock.unlock();
                }
                

                Thread.sleep(10);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(SharedVar.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public boolean isDisplay() {
        return display;
    }

    public void setDisplay(boolean display) {
        this.display = display;
    }

    public boolean isStop() {
        return stop;
    }

    public void setStop(boolean stop) {
        this.stop = stop;
    }

    public void increment() throws InterruptedException {
        
        dirty=true;
        for(int i=0;i<values.length;i++){
            values[i]++;
            Thread.sleep(10);
        }
    }

    public void setConditions(Condition r,Condition w) {
        this.readCond=r;
        this.writeCond=w;
    }

    public boolean isDirty() {
        return dirty;
    }
}
