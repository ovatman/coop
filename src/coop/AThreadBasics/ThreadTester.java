/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coop.AThreadBasics;

import java.util.Arrays;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ovatman
 */
public class ThreadTester {

    public void testCreation() throws InterruptedException {
        System.out.println("Test started");
        RunnableImplementor temp;
        Thread[] threads = new Thread[100];

        for (int i = 0; i < 50; i++) {
            threads[i] = new ThreadExtender();
            threads[i].setName("E" + i);
            threads[i].start();
        }

        for (int i = 50; i < 100; i++) {
            temp = new RunnableImplementor();
            temp.setName("T" + i);
            threads[i] = new Thread(temp);
            threads[i].setDaemon(false);
            threads[i].start();
        }

        //for(int i=0;i<100;i++)
        //    threads[i].join();
        System.out.println("Test ended");
    }

    public void testSharedVar() throws InterruptedException {
        SharedVar sv = new SharedVar();

        Thread[] threads = new Thread[100];

        for (int i = 0; i < 100; i++) {
            threads[i] = new Thread(new SharedVarThread(i, sv));
            threads[i].start();
        }

    }

    public void testSharedArray() throws InterruptedException {
        final SharedArray sarr = new SharedArray();

        //Thread sarrt = (new Thread(sarr));
        //sarrt.start();
       //sarr.setDisplay(true);
        Thread[] threads = new Thread[100];
        int i = 0;

        for (Thread t : threads) {

            t = new Thread(
                    new Runnable() {

                        @Override
                        public void run() {
                            try {
                                Thread.sleep((int) (Math.random() * 300));
                                sarr.increment();
                                System.out.println(Thread.currentThread().getName() + Arrays.toString(sarr.getValues()));
                            } catch (InterruptedException ex) {
                                Logger.getLogger(ThreadTester.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }

                    }
            );
            t.setName("T" + i++);
            t.start();
        }
    }

    public void testLockedArray() throws InterruptedException {

        final SharedLockedArray sarr = new SharedLockedArray();
        final ReentrantLock l = new ReentrantLock();
        final Condition readCond = l.newCondition();
        final Condition writeCond = l.newCondition();

        sarr.setaLock(l);
        sarr.setConditions(readCond, writeCond);

        Thread sarrt = (new Thread(sarr));
        sarrt.start();

        sarr.setDisplay(true);
        Thread.sleep(1000);

        Thread[] threads = new Thread[100];

        for (int i = 0; i < threads.length; i++) {

            threads[i] = new Thread(
                    new Runnable() {

                        @Override
                        public void run() {
                            try {

                                Thread.sleep((int) (Math.random() * 300));

                                l.lock();
                                while (sarr.isDirty()) {
                                    writeCond.await();
                                }
                                sarr.increment();
                                System.out.println(Thread.currentThread().getName() + Arrays.toString(sarr.getValues()));

                                readCond.signal();
                                l.unlock();
                            } catch (InterruptedException ex) {
                                Logger.getLogger(ThreadTester.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }

                    }
            );
            threads[i].setName("T" + i);
            threads[i].start();
        }

        for (int i = 0; i < threads.length; i++) 
            threads[i].join();
        

        sarr.setStop(true);
        sarrt.join();
    }

    private class SharedVarThread implements Runnable {

        private final String name;
        private int waitFactor;
        private SharedVar sv;

        public SharedVarThread(int waitFactor, SharedVar sv) {
            this.waitFactor = waitFactor;
            this.sv = sv;
            name = "T" + waitFactor;
        }

        public void setWaitFactor(int waitFactor) {
            this.waitFactor = waitFactor;
        }

        public void setSv(SharedVar sv) {
            this.sv = sv;
        }

        @Override
        public void run() {
            try {
                synchronized (SharedVarThread.class) {
                    sv.increment();
                    Thread.sleep((int) (Math.random() * 50));
                    System.out.println(name + "'s copy:" + sv.getValue());
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(ThreadTester.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
