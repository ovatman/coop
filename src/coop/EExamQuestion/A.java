/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coop.EExamQuestion;

/**
 *
 * @author ovatman
 */
    class A {

        private int a1;
        private int a2;
        private B b;
        
        public A() {
        }
        
        public void setB(B b){
            this.b=b;
        }
        
        public int getA1() throws InterruptedException{
            
            synchronized(A.class){
                System.out.println("get1");
                Thread.sleep(1000);
                System.out.println("get2");
                Thread.sleep(1000);
                System.out.println("get3");
            }
                return a1;
        }

        public void setA2(int val) throws InterruptedException {
            synchronized(A.class){
                System.out.println("set1");
                Thread.sleep(1000);
                System.out.println("set2");
                Thread.sleep(1000);
                System.out.println("set3");
           
                //b.getB1();
            }
        }
    }
