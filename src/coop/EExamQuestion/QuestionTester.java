/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coop.EExamQuestion;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ovatman
 */
public class QuestionTester {

    public void testQuestion() throws InterruptedException {
        final A a = new A();
        
        final B b = new B(a);
      
        a.setB(b);
        
        Thread t1 = new Thread(
                new Runnable() {

                    @Override
                    public void run() {
                        try {
                            a.setA2(3);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(QuestionTester.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
        );
        
        Thread t2 = new Thread(
                new Runnable() {

                    @Override
                    public void run() {
                        
                        try {
                            a.getA1();
                        } catch (InterruptedException ex) {
                            Logger.getLogger(QuestionTester.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
        );
        
        
        t1.start();
        t2.start();
        
       
        t1.join();
        t2.join();
    }

}
