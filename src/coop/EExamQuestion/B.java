/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coop.EExamQuestion;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author ovatman
 */
    class B {

        private int b1;
        private int b2;
        private A a;
        
        public B(A a) {
            this.a=a;
        }

        public synchronized int getB1(){
            return b1;
        }
        
        public synchronized void setB2(int val) throws InterruptedException{
            synchronized(a){
                System.out.println("B about to call A");
            
                Thread.sleep(1000);
                a.getA1();
            }
        }
    }
