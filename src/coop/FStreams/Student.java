/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coop.FStreams;

/**
 *
 * @author ovatman
 */
public class Student {
    private String name;
    private int number;

    public Student(String name, int number) {
        this.name = name;
        this.number = number;
    }
    
    public Student() {
        this.number = (int)(Math.random()*1000); 
        this.name = "Student"+number;
    }
    
    @Override
    public String toString() {
        return "Student{" + "name=" + name + ", number=" + number + "}\n";
    }

    public String getName() {
        return name;
    }

    public int getNumber() {
        return number;
    }
    
    
}
