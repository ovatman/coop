/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coop.FStreams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import static java.util.function.Function.identity;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 *
 * @author ovatman
 */
public class StreamTester {

    private List<Student> students;

    public StreamTester() {
        students = new ArrayList<Student>();

        for (int i = 0; i < 5; i++) {
            students.add(new Student());
        }
    }

    public void testStreams() {

        example1();
        //example2();
        //example3();
        //example4();
        //example5();
        //example6();
        //example7();
        //example8();
        //example9();
        //example10();
    }

    private void example1() {

        Stream<Student> studentStream = students.stream();

        studentStream.forEach(p -> System.out.println(p));
    }

    private void example2() {

        Stream<Student> studentStream = students.stream();

        List<Student> moreStudents = new LinkedList<Student>();

        Consumer<Student> adder = moreStudents::add;
        Consumer<Student> printer = System.out::println;

        Consumer<Student> addNprint = adder.andThen(printer);

        studentStream.forEach(addNprint);
        System.out.println("******************");
        moreStudents.forEach(printer);

    }

    private void example3() {
        Stream<Student> studentStream = students.stream();

        //studentStream.forEach(System.out::println); 
        students.stream().forEach(System.out::println);
        System.out.println("*********************");

        studentStream.filter(student -> student.getNumber() < 300)
                .forEach(System.out::println);

        System.out.println("*********************");

        Predicate<Student> nameFilter = student -> student.getName().charAt(student.getName().length() - 1) > '5';
        students.stream().filter(nameFilter).forEach(System.out::println);

    }

    private void example4() {

        Predicate<Student> more = student -> student.getNumber() > 300;
        Predicate<Student> less = student -> student.getNumber() < 600;

        students.stream().filter(more.and(less)).forEach(System.out::println);
        System.out.println("******************");

        students.stream().filter(more.or(less)).forEach(System.out::println);
        System.out.println("******************");

        students.stream().filter((more.and(less)).negate()).forEach(System.out::println);

        System.out.println("******************");
        students.stream().filter(more).peek(System.out::println).filter(less).forEach(System.out::println);

    }

    private void example5() {

        Double[] nums = {1.0, 2.0, 3.0, 4.0, 5.0};
        Double[] squares  = Stream.of(nums).map(n -> n * n).toArray(Double[]::new);
        System.out.println(Arrays.toString(squares));
        System.out.println("****************************");
        Stream.of(squares).map(Math::sqrt).forEach(System.out::println);
        System.out.println("****************************");
        students.stream().map(Student::getName).forEach(System.out::println);
    }

    private void example6() {
        Integer[] nums = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
        
        
        int maxVal = Stream.of(nums).reduce(Integer.MIN_VALUE,Integer::max).intValue();
        System.out.println("Maximum:"+maxVal);

        int minVal = Stream.of(nums).reduce(Integer.MAX_VALUE,Integer::min).intValue();
        System.out.println("Minimum:"+minVal);
        
        
        int sum = Stream.of(nums).reduce(0,(n1,n2) -> n1+n2).intValue();
        System.out.println("Sum:"+sum);
        
        Student bart = new Student("Bart Simpson", -1);
        
        BinaryOperator<Student> largerNum = (s1, s2) -> {
            return(s1.getNumber() >= s2.getNumber() ? s1 : s2);
        };
        
        Student largest = students.stream().reduce(bart,largerNum);
        System.out.println(largest);
        
        int sumStudentNumbers = students.stream().mapToInt(Student::getNumber).sum();
        
        System.out.println("Sum of student numbers:"+sumStudentNumbers);
    }
    
    private void example7() {
        
        students = new ArrayList<Student>();
        
        students.add(new Student("Tolga",100));
        students.add(new Student("Vehbi",500));
        students.add(new Student("Duygu",200));
        students.add(new Student("Ozlem",300));
        students.add(new Student("Nur",400));
        students.add(new Student("Cenk",600));
        
        
        Map<Integer,Student> studentMap;
        studentMap = students.stream().collect(Collectors.toMap(Student::getNumber,Function.identity()));
        
        System.out.println(studentMap);
        System.out.println("******************************");
        
       List<Integer> ids = Arrays.asList(100,300,400); 
       List<Student> selectedStudents = ids.stream().map(studentMap::get)
              .sorted((e1, e2) -> e1.getName().compareTo(e2.getName()))
              .collect(Collectors.toList());
       
        System.out.println(selectedStudents);
    }
    
    private void example8() {
        String twister= "Peter Piper picked a peck of pickled peppers " +
                        "A peck of pickled peppers Peter Piper picked " +
                        "If Peter Piper picked a peck of pickled peppers " +
                        "Where is the peck of pickled peppers Peter Piper picked";
    
        Map<String,Long> freq;
        freq = Stream.of(twister.split(" ")).collect(groupingBy(identity(),counting()));
        
       
        System.out.println(freq);
        
        
        
    }

    private void example9() {
        
        students.stream().parallel().forEach(p -> System.out.println(p));
        ArrayList<Integer> nums = new ArrayList<Integer>();
        
        for(int i=0;i<50000000;i++)
            nums.add(i);
        
        long start= System.currentTimeMillis();
        
        long sum=nums.stream().mapToInt(w->w).sum();
        
        
        System.out.println("Sum:"+sum);
        System.out.println("Time passed:"+(System.currentTimeMillis()-start));
    }

    private void example10() {
        Stream.generate(() -> randomNumber()).limit(10).forEach(System.out::println);
    }

    private Integer randomNumber() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(StreamTester.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return ((int) (Math.random()*100));
    
    }
    

    

    

}
