/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coop.CRegisters;

/**
 *
 * @author ovatman
 */
public interface Register<T> {
    public void setValue(T val) throws InterruptedException;
    public T getValue();
    
}
