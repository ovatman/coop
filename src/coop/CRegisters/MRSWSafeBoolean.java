/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coop.CRegisters;

/**
 *
 * @author ovatman
 */
public class MRSWSafeBoolean implements Register<Boolean> {
     private boolean[] readers;

    public MRSWSafeBoolean() {
        readers = new boolean[4];
    }

    @Override
    public void setValue(Boolean val) throws InterruptedException {
        for (int j = 0; j < 4; j++){
            Thread.sleep(100+(int)(Math.random()*500));
            readers[j]=val;
        }
    }

    @Override
    public Boolean getValue() {

        int i = (int) Thread.currentThread().getId()%4;
        return readers[i];
    }
}
