/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coop.CRegisters;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ovatman
 */
public class MRSWRegularRegister implements Register<Integer> {

    private MRSWRegularBoolean[] bit;

    public MRSWRegularRegister() {
        bit = new MRSWRegularBoolean[16];

        for (int i = 0; i < 16; i++) {
            bit[i] = new MRSWRegularBoolean();
        }
    }

    @Override
    public void setValue(Integer val) throws InterruptedException {
        this.bit[val].setValue(true);
        for (int i = val - 1; i >= 0; i--) {
            this.bit[i].setValue(false);
        }
    }

    @Override
    public Integer getValue() {

        for (int i = 0; i < 16; i++) {
            try {
                Thread.sleep(100);
                if (this.bit[i].getValue()) {
                    return i;
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(MRSWRegularRegister.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return -1;
    }

}
