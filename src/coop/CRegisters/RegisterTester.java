/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coop.CRegisters;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ovatman
 */
public class RegisterTester {

    public void testRegister() throws InterruptedException {
        final Register reg= new MRSWNotSafeRegister();
        //final Register reg= new MRSWSafeBoolean();
        //final Register reg= new MRSWRegularBoolean();
        //final Register reg= new MRSWRegularRegister();
        
        int N = 5;
        ExecutorService e = Executors.newFixedThreadPool(N);

        for(int i=0;i<N-1;i++)
            e.execute(new Runnable(){

                @Override
                public void run() {
                    int iter=0;
                    while(iter<100){
                        try {
                            Thread.sleep((int)(Math.random()*500));
                            System.out.println(Thread.currentThread().getId()+":"+reg.getValue());
                        } catch (InterruptedException ex) {
                            Logger.getLogger(RegisterTester.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        iter++;
                    }
                }
            
            });
        
        e.execute(new Runnable(){

            @Override
            public void run() {
                int iter=0;
                    while(iter<10){
                        try {
                            int i=5+(int)(Math.random()*5);
                            //Boolean i = Math.random()>0.5?true:false;
                            System.out.println("Writer will write:"+i);
                            reg.setValue(i);
                            System.out.println("Writer wrote:"+i);
                            Thread.sleep(1000);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(RegisterTester.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        iter++;
                    }
            }
        
        });
        
        e.shutdown();
    }
    
}
