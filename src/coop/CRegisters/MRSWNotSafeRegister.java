/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coop.CRegisters;

/**
 *
 * @author ovatman
 */
public class MRSWNotSafeRegister implements Register<Integer>{
    private char[] bits;

    public MRSWNotSafeRegister() {
        bits = new char[]{'0','0','0','0'};
    }
    
    @Override
    public Integer getValue(){
        return Integer.parseInt(String.valueOf(bits).trim(), 2);
    }
    
    @Override
    public void setValue(Integer val) throws InterruptedException{
        String binStr=Integer.toBinaryString(val);
        
        int i;
        int j;
        for(i=0,j=bits.length-binStr.length();i<binStr.length();i++,j++){
            Thread.sleep(100+(int)(Math.random()*500));
            bits[j]=binStr.charAt(i);
        }
        
        for(j=0;j<bits.length-binStr.length();j++){
            Thread.sleep(100+(int)(Math.random()*500));
            bits[j]='0';
        }
        
        
    }
    
}
