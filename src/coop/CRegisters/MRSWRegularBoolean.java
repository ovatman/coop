/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coop.CRegisters;

/**
 *
 * @author ovatman
 */
public class MRSWRegularBoolean implements Register<Boolean> {

    private ThreadLocal<Boolean> old;
    private MRSWSafeBoolean value;

    public MRSWRegularBoolean() {
        old = new ThreadLocal<Boolean>();
        value=new MRSWSafeBoolean();
    }

    @Override
    public void setValue(Boolean val) throws InterruptedException {
        if (old.get() != val) {
            value.setValue(val);
            old.set(val);
        }
    }

    @Override
    public Boolean getValue() {
        return value.getValue();
    }

}
